/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;

/**
 *
 * @author progmatic
 */
public class WallFollowerPlayer implements Player {

    Direction followDirection;
    Direction unFollow;
    int moves = 0;

    @Override
    public Direction nextMove(Labyrinth l) {
        moves++;
        if (moves == 1) {

            if (!l.possibleMoves().contains(Direction.EAST)) {
                followDirection = Direction.EAST;
                unFollow = Direction.WEST;

            }
            if (!l.possibleMoves().contains(Direction.WEST)) {
                followDirection = Direction.WEST;
                unFollow = Direction.EAST;

            }
            if (!l.possibleMoves().contains(Direction.SOUTH)) {
                followDirection = Direction.SOUTH;
                unFollow = Direction.NORTH;

            }
            if (!l.possibleMoves().contains(Direction.NORTH)) {
                followDirection = Direction.NORTH;
                unFollow = Direction.SOUTH;

            }

        }
        if (l.possibleMoves().contains(followDirection)) {
            return followDirection;
        }
        if (l.possibleMoves().size() == 1) {
            return l.possibleMoves().get(0);
        } else {
            for (Direction d : l.possibleMoves()) {
                if (d != unFollow) {
                    return d;
                }
            }
        }
        return null;
    }

}
