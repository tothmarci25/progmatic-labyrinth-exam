/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class ConsciousPlayer implements Player {

    Coordinate endCoord;
    Coordinate playerPos;
    int height;
    int width;
    CellType[][] labyrint;
    List<Direction> route = new ArrayList<>();
    boolean alredyMoved = false;
    int moves = 0;
    List<Direction> perfectRoute = new ArrayList<>();

    public ConsciousPlayer() {

    }

    @Override
    public Direction nextMove(Labyrinth l) {
        int next = moves;
        if (moves == 0) {

            this.playerPos = l.getPlayerPosition();
            this.height = l.getHeight();
            this.width = l.getWidth();
            LabyrinthImpl la = (LabyrinthImpl) l;
            this.labyrint = la.getLabyrint();
            this.endCoord = la.getEndCoord();
            perfectRoute = this.lookForPerfectRoute(this.playerPos, null, route);
            moves++;
            return perfectRoute.get(next);

        } else {
            moves++;
            return perfectRoute.get(next);

        }

    }

    private List<Direction> possibleMoves(Coordinate playerPos, Direction prevMove) {
        List<Direction> posible = new ArrayList<>();
        if (prevMove != null) {
            if (prevMove != Direction.WEST && playerPos.getCol() + 1 < this.width && playerPos.getCol() + 1 >= 0 && labyrint[playerPos.getRow()][playerPos.getCol() + 1] != CellType.WALL) {
                posible.add(Direction.EAST);
            }
            if (prevMove != Direction.EAST && playerPos.getCol() - 1 >= 0 && playerPos.getCol() - 1 < this.width && labyrint[playerPos.getRow()][playerPos.getCol() - 1] != CellType.WALL) {
                posible.add(Direction.WEST);
            }
            if (prevMove != Direction.NORTH && playerPos.getRow() + 1 < this.height && playerPos.getRow() + 1 >= 0 && labyrint[playerPos.getRow() + 1][playerPos.getCol()] != CellType.WALL) {
                posible.add(Direction.SOUTH);
            }
            if (prevMove != Direction.SOUTH && playerPos.getRow() - 1 >= 0 && playerPos.getRow() - 1 < this.height && labyrint[playerPos.getRow() - 1][playerPos.getCol()] != CellType.WALL) {
                posible.add(Direction.NORTH);
            }
        } else {
            if (playerPos.getCol() + 1 < this.width && playerPos.getCol() + 1 >= 0 && labyrint[playerPos.getRow()][playerPos.getCol() + 1] != CellType.WALL) {
                posible.add(Direction.EAST);
            }
            if (playerPos.getCol() - 1 >= 0 && playerPos.getCol() - 1 < this.width && labyrint[playerPos.getRow()][playerPos.getCol() - 1] != CellType.WALL) {
                posible.add(Direction.WEST);
            }
            if (playerPos.getRow() + 1 < this.height && playerPos.getRow() + 1 >= 0 && labyrint[playerPos.getRow() + 1][playerPos.getCol()] != CellType.WALL) {
                posible.add(Direction.SOUTH);
            }
            if (playerPos.getRow() - 1 >= 0 && playerPos.getRow() - 1 < this.height && labyrint[playerPos.getRow() - 1][playerPos.getCol()] != CellType.WALL) {
                posible.add(Direction.NORTH);
            }
        }
        return posible;
    }

    private List<Direction> lookForPerfectRoute(Coordinate actual, Direction prev, List<Direction> rout) {
        Coordinate actualPos = actual;
        Direction prevDirection = prev;
        List<Direction> routeSofar = rout;

        while (!actualPos.equals(this.endCoord)) {
            List<Direction> possibles = this.possibleMoves(actualPos, prevDirection);
            if (possibles.size() == 1) {
                Direction nextDirection = possibles.get(0);
                actualPos = setActualPos(actualPos, nextDirection);
                routeSofar.add(nextDirection);
                prevDirection = nextDirection;

            } else if (possibles.isEmpty()) {
                return null;
            } else {
                for (int i = 0; i < possibles.size(); i++) {

                    if (i == possibles.size() - 1) {
                        Direction nextDirection = possibles.get(i);
                        actualPos = setActualPos(actualPos, nextDirection);
                        routeSofar.add(nextDirection);
                        prevDirection = nextDirection;
                    } else {
                        Coordinate nextActual = setActualPos(actualPos, possibles.get(i));
                        List<Direction> routeCont = new ArrayList<>();
                        routeCont.addAll(routeSofar);
                        routeCont.add(possibles.get(i));
                        List<Direction> conted = lookForPerfectRoute(nextActual, possibles.get(i), routeCont);
                        if (conted != null) {
                            return conted;
                        }
                    }
                }
            }
        }
        return routeSofar;
    }

    private Coordinate setActualPos(Coordinate actualPos, Direction d) {
        if (d == Direction.EAST) {
            return new Coordinate(actualPos.getCol() + 1, actualPos.getRow());
        }
        if (d == Direction.WEST) {
            return new Coordinate(actualPos.getCol() - 1, actualPos.getRow());
        }
        if (d == Direction.NORTH) {
            return new Coordinate(actualPos.getCol(), actualPos.getRow() - 1);
        }
        if (d == Direction.SOUTH) {
            return new Coordinate(actualPos.getCol(), actualPos.getRow() + 1);
        }
        return null;
    }

}
