package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    CellType[][] labyrint;
    int width;
    int height;
    Player p;
    Coordinate playerPos;
    Coordinate endCoord;

    public LabyrinthImpl() {

    }

    public CellType[][] getLabyrint() {
        return labyrint;
    }

    public Coordinate getEndCoord() {
        return endCoord;
    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            width = Integer.parseInt(sc.nextLine());
            height = Integer.parseInt(sc.nextLine());
            labyrint = new CellType[height][width];

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            labyrint[hh][ww] = CellType.WALL;
                            break;
                        case 'E':
                            labyrint[hh][ww] = CellType.END;
                            endCoord = new Coordinate(ww, hh);
                            break;
                        case 'S':
                            labyrint[hh][ww] = CellType.START;
                            this.playerPos = new Coordinate(ww, hh);
                            break;
                        case ' ':
                            labyrint[hh][ww] = CellType.EMPTY;
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public int getWidth() {
        if (this.width == 0) {
            return -1;
        } else {
            return this.width;
        }
    }

    @Override
    public int getHeight() {
        if (this.height == 0) {
            return -1;
        } else {
            return this.height;
        }
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {

        if (c.getCol() > this.width - 1 || c.getCol() < 0 || c.getRow() > this.height - 1 || c.getRow() < 0) {
            throw new CellException(c, "Nem létező koordináta");
        }
        if (this.labyrint[c.getRow()][c.getCol()] == CellType.START) {
            return CellType.START;
        } else if (this.labyrint[c.getRow()][c.getCol()] == CellType.END) {
            return CellType.END;
        } else if (this.labyrint[c.getRow()][c.getCol()] == CellType.WALL) {
            return CellType.WALL;
        } else {
            return CellType.EMPTY;
        }
    }

    @Override
    public void setSize(int width, int height) {
        this.labyrint = new CellType[height][width];
        this.width = width;
        this.height = height;

    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        if (c.getCol() > this.width || c.getRow() > this.height) {
            throw new CellException(c, "Nem létező koordináta");
        }
        if (type == CellType.START) {
            this.playerPos = c;
            this.labyrint[c.getRow()][c.getCol()] = type;
        } else if (type == CellType.END) {
            this.endCoord = c;
            this.labyrint[c.getRow()][c.getCol()] = type;
        } else {

            this.labyrint[c.getRow()][c.getCol()] = type;
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
        return this.playerPos;
    }

    @Override
    public boolean hasPlayerFinished() {
        if (this.playerPos.equals(this.endCoord)) {
            return true;
        } else {
            return false;

        }
    }

    @Override
    public List<Direction> possibleMoves() {
        List<Direction> posible = new ArrayList<>();

        if (playerPos.getCol() + 1 < this.width && playerPos.getCol() + 1 >= 0 && labyrint[playerPos.getRow()][playerPos.getCol() + 1] != CellType.WALL) {
            posible.add(Direction.EAST);
        }
        if (playerPos.getCol() - 1 >= 0 && playerPos.getCol() - 1 < this.width && labyrint[playerPos.getRow()][playerPos.getCol() - 1] != CellType.WALL) {
            posible.add(Direction.WEST);
        }
        if (playerPos.getRow() + 1 < this.height && playerPos.getRow() + 1 >= 0 && labyrint[playerPos.getRow() + 1][playerPos.getCol()] != CellType.WALL) {
            posible.add(Direction.SOUTH);
        }
        if (playerPos.getRow() - 1 >= 0 && playerPos.getRow() - 1 < this.height && labyrint[playerPos.getRow() - 1][playerPos.getCol()] != CellType.WALL) {
            posible.add(Direction.NORTH);
        }

        return posible;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {
        List<Direction> possibles = this.possibleMoves();
        if (!this.possibleMoves().contains(direction)) {
            throw new InvalidMoveException("Erre a pozícióra nem tudsz lépni");
        }
        if (direction == Direction.EAST) {
            Coordinate playersNewCoord = new Coordinate(this.playerPos.getCol() + 1, this.playerPos.getRow());
            this.playerPos = playersNewCoord;
        }
        if (direction == Direction.WEST) {
            Coordinate playersNewCoord = new Coordinate(this.playerPos.getCol() - 1, this.playerPos.getRow());
            this.playerPos = playersNewCoord;
        }
        if (direction == Direction.SOUTH) {
            Coordinate playersNewCoord = new Coordinate(this.playerPos.getCol(), this.playerPos.getRow() + 1);
            this.playerPos = playersNewCoord;
        }
        if (direction == Direction.NORTH) {
            Coordinate playersNewCoord = new Coordinate(this.playerPos.getCol(), this.playerPos.getRow() - 1);
            this.playerPos = playersNewCoord;
        }

    }

}
